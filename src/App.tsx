import "./App.css";
import NavBar from "./components/NavBar";
import logoImage from "./images/logo.png";

import { Routes, Route } from "react-router-dom";

import Users from "./pages/Users";
import About from "./pages/About";
import Home from "./pages/Home";

import "./fonts/Futura-Regular.otf";
import "./fonts/goodmorningpurple_1.2_free.ttf";
import "./fonts/Rotulista-Regular.ttf";

const App = () => {
  return (
    <>
      <NavBar img={logoImage} />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/users" element={<Users />} />
      </Routes>
    </>
  );
};

export default App;
