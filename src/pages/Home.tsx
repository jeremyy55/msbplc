import HeroHeader from "../components/HeroHeader";
import Festival from "../components/Festival";
import MsbplcBrief from "../components/MsbplcBrief";

function Home() {
  return (
    <>
      <HeroHeader />
      <Festival />
      <MsbplcBrief />

      <h1>Licences</h1>
      <ul>
        <li>
          Image de{" "}
          <a href="https://fr.freepik.com/search?format=search&query=sustainability%20world">
            Freepik
          </a>
        </li>
        <li>
          <a href="https://fr.freepik.com/photos-gratuite/diy-agite-fond-texture-dans-art-abstrait-experimental-vert_17592267.htm#query=wave%20draw%20green&position=10&from_view=search&track=ais">
            Image de rawpixel.com
          </a>{" "}
          sur Freepik
        </li>
        <li>
          <a href="https://fr.freepik.com/vecteurs-libre/personnes-agees-travaillant-dans-illustration-du-jardin_20827888.htm#&position=5&from_view=undefined">
            Image de pch.vector
          </a>{" "}
          sur Freepik
        </li>
      </ul>
    </>
  );
}
export default Home;
