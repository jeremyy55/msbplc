import styles from "../styles/MsbplcBrief.module.css";
import image from "../images/chill.jpeg";

const MsbplcBrief = () => {
  return (
    <div
      className={styles.containerFullscreen}
      style={{ backgroundImage: `url(${image})` }}
    >
      <div className={styles.childrenLocation}>
        <h1>Mons se bouge pour le climat, c'est quoi ?</h1>
        <h2>
          Msbplc, c'est un collectif indépendant formé par des citoyens de Mons.
        </h2>
        <h2>Notre objectif est multiple:</h2>
        <ul>
          <li>
            Sensibiliser les citoyens de Mons quant aux questions climatiques.
          </li>
          <li> Relayer les initiatives autant que les luttes locales</li>
          <li>
            Connecter les citoyens entre eux en facilitant l'accès aux gestes
            qui font écho à leur valeurs
          </li>
        </ul>
      </div>
    </div>
  );
};

export default MsbplcBrief;
