import styles from "../styles/Festival.module.css";
import image from "../images/oui.jpg";

const Festival = () => {
  return (
    <div
      className={`${styles.containerFullscreen} ${styles.darker}`}
      style={{ backgroundImage: `url(${image})` }}
    >
      <div>
        <h1 className={styles.titleFestival}>Un autre Mons est possible</h1>
        <h2>
          Le festival est de retour pour une deuxième édition digne de ce nom
        </h2>
        <ul className={styles.listWhouaw}>
          <li>Quoi? Conférences, ateliers, actions et concert</li>
          <li>
            Quand? Début septembre.
            <span className={styles.petit}> la date précise arrive</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Festival;
