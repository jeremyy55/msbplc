import React from "react";
import styles from "../styles/HeroHeader.module.css";
import image from "../images/heroImage.jpeg";

const HeroHeader = () => {
  return (
    <>
      <div
        className={styles.containerHero}
        style={{ backgroundImage: `url(${image})` }}
      >
        <div className={styles.HeroTextDiv}>
          <h1>
            Tu vis près de Mons et tu te sens concerné par la crise écologique?
          </h1>
          <p>
            Mons dispose de nombreuses alternatives:
            <br />
            <span className={styles.highlight}> Ensemble</span> , renforçons
            les!
          </p>
        </div>
      </div>
    </>
  );
};

export default HeroHeader;
