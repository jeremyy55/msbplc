import { Link } from "react-router-dom";
import styles from "../styles/NavBar.module.css";
import useScrollUp from "../effects/UseScrollUp";
import { useState } from "react";
import imgOff from "../images/drop-off.svg";
import imgOn from "../images/drop-on.svg";

interface Props {
  img?: string;
  alt?: string;
}

const NavBar = ({ img, alt = "logo navbar" }: Props) => {
  const scrollUp = useScrollUp();

  return (
    <>
      <div
        style={
          scrollUp
            ? {
                transition: "transform 1s  ",
                transform: "scaleY(0) ",
                transformOrigin: "top center",
              }
            : {
                transition: "transform 1s  ",
                transform: "scaleY(1)",
                transformOrigin: "top center",
              }
        }
        className={styles.containerNav}
      >
        <ActivatedNavBar img={img} />
      </div>
    </>
  );
};

const ActivatedNavBar = ({ img, alt }: Props) => {
  return (
    <nav className={styles.activatedNavBar}>
      <div className={styles.logoContainer}>
        <img className={styles.logo} src={img} alt={alt} />
      </div>
      <ul>
        <li>
          <Link className={styles.link} to="/">
            Accueil
          </Link>
        </li>
        <li>
          <Link to="/about" className={styles.link}>
            À Propos
          </Link>
        </li>
        <li>
          <Link to="/users" className={styles.link}>
            Users Zone
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
