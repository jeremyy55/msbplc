import { useState, useEffect } from "react";

function useScrollUp() {
  const [scrollUp, setScrollUp] = useState(false);

  useEffect(() => {
    let lastScrollY = window.pageYOffset;

    const updateScrollUp = () => {
      const scrollY = window.pageYOffset;
      const direction = scrollY > lastScrollY ? true : false;
      if (
        direction !== scrollUp &&
        (scrollY - lastScrollY > 10 || scrollY - lastScrollY < -10)
      ) {
        setScrollUp(direction);
      }
      lastScrollY = scrollY > 0 ? scrollY : 0;
    };
    window.addEventListener("scroll", updateScrollUp); // add event listener
    return () => {
      window.removeEventListener("scroll", updateScrollUp); // clean up
    };
  }, [scrollUp]);

  return scrollUp;
}

export default useScrollUp;
